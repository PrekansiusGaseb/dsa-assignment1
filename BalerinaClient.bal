import ballerina/http;

function addLecturer(string staffNumber, string officeNumber, string staffName, string title, string[] courses) returns http:Response {
    // Define the base URL of the API
    string baseUrl = "http://api.example.com";

    // Create a JSON payload for the new lecturer
    json payload = {
        "staffNumber": staffNumber,
        "officeNumber": officeNumber,
        "staffName": staffName,
        "title": title,
        "courses": courses
    };

    // Create an HTTP request to add a new lecturer
    http:Request addLecturerRequest = new;
    addLecturerRequest.setPayload(payload.toString());
    addLecturerRequest.setUri(baseUrl + "/lecturers");
    addLecturerRequest.setMethod("POST");

    // Send the HTTP request and return the response
    http:Response addLecturerResponse = http:ClientConnector { }.post(addLecturerRequest);
    return addLecturerResponse;
}

public function main() {
    // Define lecturer details
    string staffNumber = "001";
    string officeNumber = "A101";
    string staffName = "John Doe";
    string title = "Lecturer";
    string[] courses = ["CS101", "CS202"];

    // Add a new lecturer and get the response
    http:Response response = addLecturer(staffNumber, officeNumber, staffName, title, courses);

    // Check the response status code
    if (response.statusCode == 201) {
        io:println("Lecturer added successfully");
    } else {
        io:println("Failed to add lecturer: " + response.getTextPayload());
    }

    // Implement other API interactions here (GET, PUT, DELETE, etc.)
}
import ballerina/grpc;
import ballerina/io;

// Define a sample User and Book
message User {
    string user_id = 1;
    string profile = 2;
}

message Book {
    string title = 1;
    string author_1 = 2;
    string author_2 = 3;
    string location = 4;
    string isbn = 5;
    bool status = 6;
}

function main() {
    // Create a gRPC client to connect to the server
    grpc:Client libraryClient = new ("localhost:8080", LibraryService);

    // Call gRPC service methods and handle responses here

    // Add a book
    var addBookResponse = check libraryClient->AddBook({
        title: "Sample Book",
        author_1: "Author A",
        author_2: "Author B",
        location: "Shelf 1",
        isbn: "1234567890",
        status: true
    });
    match addBookResponse {
        string isbn => io:println("Added book with ISBN: " + isbn);
        error e => io:println("Error adding book: " + e.message());
    }

    // Update a book
    var updatedBook = check libraryClient->UpdateBook({
        title: "Updated Book",
        author_1: "New Author",
        author_2: "",
        location: "Shelf 2",
        isbn: "1234567890",
        status: true
    });
    match updatedBook {
        Book book => io:println("Updated book details: " + book);
        error e => io:println("Error updating book: " + e.message());
    }

    // Remove a book
    var removeBookResponse = check libraryClient->RemoveBook("1234567890");
    match removeBookResponse {
        Book[] books => io:println("Removed book with ISBN: " + books[0].isbn);
        error e => io:println("Error removing book: " + e.message());
    }

    // List available books
    var listAvailableBooksResponse = libraryClient->ListAvailableBooks();
    _ = listAvailableBooksResponse.forEach(function (Book book) {
        io:println("Available book: " + book.title);
    });

    // Locate a book
    var locateBookResponse = check libraryClient->LocateBook("1234567890");
    match locateBookResponse {
        string location => io:println(location);
        error e => io:println("Error locating book: " + e.message());
    }

    // Borrow a book (sample user)
    var borrowBookResponse = check libraryClient->BorrowBook({
        user_id: "123",
        profile: "Student"
    });
    match borrowBookResponse {
        string message => io:println(message);
        error e => io:println("Error borrowing book: " + e.message());
    }
}
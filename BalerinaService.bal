import ballerina/http;

// Define an in-memory data store for lecturers
map<string> lecturerStore = {};

// Define the service to handle lecturer-related endpoints
service /lecturers on new http:Listener(8080) {
    
    // Resource to retrieve a list of all lecturers
    resource function getAllLecturers() returns json {
        // Retrieve and return a list of all lecturers
        json[] lecturers = lecturerStore.values();
        return lecturers;
    }

    // Resource to add a new lecturer
    resource function addLecturer(json lecturer) returns json {
        // Generate a unique staff number (you can use a more robust approach)
        string staffNumber = lecturer.staffNumber.toString();
        
        // Check if the lecturer already exists
        if (lecturerStore.hasKey(staffNumber)) {
            error errMsg = {message: "Lecturer with staff number already exists"};
            http:Response errorResponse = new;
            errorResponse.statusCode = 400;
            errorResponse.setJsonPayload(errMsg);
            return errorResponse;
        }
        
        // Add the new lecturer to the data store
        lecturerStore[staffNumber] = lecturer;
        
        // Return the added lecturer
        return lecturer;
    }

    // Resource to retrieve a lecturer by staff number
    resource function getLecturerByStaffNumber(string staffNumber) returns json {
        // Check if the lecturer exists
        if (!lecturerStore.hasKey(staffNumber)) {
            error errMsg = {message: "Lecturer not found"};
            http:Response errorResponse = new;
            errorResponse.statusCode = 404;
            errorResponse.setJsonPayload(errMsg);
            return errorResponse;
        }
        
        // Retrieve and return the lecturer
        json lecturer = lecturerStore[staffNumber];
        return lecturer;
    }

    // Resource to update a lecturer's information by staff number
    resource function updateLecturer(string staffNumber, json lecturer) returns json {
        // Check if the lecturer exists
        if (!lecturerStore.hasKey(staffNumber)) {
            error errMsg = {message: "Lecturer not found"};
            http:Response errorResponse = new;
            errorResponse.statusCode = 404;
            errorResponse.setJsonPayload(errMsg);
            return errorResponse;
        }
        
        // Update the lecturer's information
        lecturerStore[staffNumber] = lecturer;
        
        // Return the updated lecturer
        return lecturer;
    }

    // Resource to delete a lecturer by staff number
    resource function deleteLecturer(string staffNumber) returns error {
        // Check if the lecturer exists
        if (!lecturerStore.hasKey(staffNumber)) {
            error errMsg = {message: "Lecturer not found"};
            http:Response errorResponse = new;
            errorResponse.statusCode = 404;
            errorResponse.setJsonPayload(errMsg);
            return errorResponse;
        }
        
        // Delete the lecturer
        lecturerStore.remove(staffNumber);
        
        // Return success
        return ();
    }

    // Implement other API endpoints here (GET, PUT, DELETE, etc.)
}
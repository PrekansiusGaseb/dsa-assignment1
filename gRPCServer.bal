import ballerina/grpc;
import ballerina/io;

// Define a map to store books (ISBN as the key)
map<string> books = {};

service LibraryService bind grpc:ListenerConfig {
    // Implement the gRPC service methods
    resource function AddBook(Book book) returns string {
        // Generate a unique ISBN (you can use a more robust approach)
        string isbn = book.isbn;

        // Check if the book already exists
        if (books.hasKey(isbn)) {
            // Book with the same ISBN already exists
            return "Book with ISBN " + isbn + " already exists";
        }

        // Add the book to the map
        books[isbn] = book;

        // Return the ISBN of the added book
        return isbn;
    }

    resource function CreateUsers(stream<User> users) returns google.protobuf.Empty {
        // Implement logic to create multiple users
        // This can be customized as needed
        foreach User user in users {
            io:println("Created user: " + user.user_id + " with profile: " + user.profile);
        }

        // Return an empty response
        return {};
    }

    resource function UpdateBook(Book book) returns Book {
        // Check if the book exists
        string isbn = book.isbn;
        if (!books.hasKey(isbn)) {
            // Book not found
            checkpanic("Book with ISBN " + isbn + " not found");
        }

        // Update the book
        books[isbn] = book;

        // Return the updated book
        return book;
    }

    resource function RemoveBook(string isbn) returns (Book[] booksRemoved) {
        // Check if the book exists
        if (!books.hasKey(isbn)) {
            // Book not found
            checkpanic("Book with ISBN " + isbn + " not found");
        }

        // Remove the book from the map and add it to the list of removed books
        booksRemoved = [books[isbn]];
        books.remove(isbn);

        // Return the list of removed books
        return booksRemoved;
    }

    resource function ListAvailableBooks() returns stream<Book> {
        // Stream all available books
        foreach key, value in books {
            if (value.status) {
                // Book is available
                _ = grpc:forward({value});
            }
        }
    }

    resource function LocateBook(string isbn) returns string {
        // Check if the book exists
        if (!books.hasKey(isbn)) {
            // Book not found
            return "Book with ISBN " + isbn + " not available";
        }

        // Check if the book is available
        if (books[isbn].status) {
            return "Book with ISBN " + isbn + " is located at " + books[isbn].location;
        } else {
            return "Book with ISBN " + isbn + " is not available";
        }
    }

    resource function BorrowBook(User user) returns string {
        // Custom logic to handle book borrowing
        // You can implement the logic to update the book's status here
        // based on the user's ID and book's ISBN

        return "Borrowed book successfully"; // Modify as needed
    }
}

function main() {
    // Start the gRPC server
    grpc:Listener listener = new (8080);
    checkpanic listener.start();
    io:println("gRPC server started on port 8080");
    listener.blockUntilShutdown();
}

// Define the Book and User message types here
message Book {
    string title = 1;
    string author_1 = 2;
    string author_2 = 3;
    string location = 4;
    string isbn = 5;
    bool status = 6;
}

message User {
    string user_id = 1;
    string profile = 2;
}